# UnsubscribedContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**phone** | **string** |  | 
**unsubscribeTime** | [**\DateTime**](\DateTime.md) |  | 
**firstName** | **string** |  | 
**lastName** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


