# UpdateSurveyNodeInputObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nodeType** | **string** | Node type. \\&#39;a\\&#39; answer; \\&#39;q\\&#39; - question | [optional] 
**label** | **string** | Node label | [optional] 
**isEndNode** | **bool** | Define node is ending for survey. Default is false | [optional] 
**body** | **string** | Node body | [optional] 
**startNode** | **int** | Start node id | [optional] 
**sendDelay** | **int** | Define delay for sending question to recipients after previous answer. Default is 0 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


