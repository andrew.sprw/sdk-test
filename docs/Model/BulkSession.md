# BulkSession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**status** | **string** |  | 
**itemsProcessed** | **int** |  | 
**itemsTotal** | **int** |  | 
**createdAt** | [**\DateTime**](\DateTime.md) |  | 
**session** | [**\text-magic\Model\MessageSession**](MessageSession.md) |  | 
**text** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


