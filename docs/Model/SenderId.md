# SenderId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**senderId** | **string** |  | 
**user** | [**\text-magic\Model\User**](User.md) |  | 
**status** | **string** | A - accepted, R - rejected, P - pending | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


