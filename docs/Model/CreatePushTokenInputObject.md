# CreatePushTokenInputObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceId** | **string** | Device ID for push notification service | 
**pushServiceType** | **string** | Push notification service type: a for Apple Push Notifications, g for Google Cloud Messaging | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


