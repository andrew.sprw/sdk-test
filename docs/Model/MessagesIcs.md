# MessagesIcs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**nextSend** | [**\DateTime**](\DateTime.md) |  | 
**rrule** | **string** |  | 
**session** | [**\text-magic\Model\MessageSession**](MessageSession.md) |  | 
**lastSent** | [**\DateTime**](\DateTime.md) |  | 
**contactName** | **string** |  | 
**parameters** | [**\text-magic\Model\MessagesIcsParameters**](MessagesIcsParameters.md) |  | 
**type** | **string** |  | 
**summary** | **string** |  | 
**textParameters** | [**\text-magic\Model\MessagesIcsTextParameters**](MessagesIcsTextParameters.md) |  | 
**firstOccurrence** | [**\DateTime**](\DateTime.md) |  | 
**lastOccurrence** | [**\DateTime**](\DateTime.md) |  | 
**recipientsCount** | **int** |  | 
**timezone** | **string** |  | 
**completed** | **bool** |  | 
**avatar** | **string** |  | 
**createdAt** | [**\DateTime**](\DateTime.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


