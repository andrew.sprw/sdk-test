# Chat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**originalId** | **int** |  | 
**phone** | **string** |  | 
**contact** | [**\text-magic\Model\Contact**](Contact.md) |  | 
**unsubscribedContactId** | **int** |  | 
**unread** | **int** |  | 
**updatedAt** | [**\DateTime**](\DateTime.md) |  | 
**status** | **string** |  | 
**mute** | **int** |  | 
**lastMessage** | **string** |  | 
**direction** | **string** |  | 
**from** | **string** |  | 
**mutedUntil** | **string** |  | 
**timeLeftMute** | **int** |  | 
**country** | [**\text-magic\Model\Country**](Country.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


