# Conversation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**direction** | **string** |  | 
**sender** | **string** |  | 
**messageTime** | [**\DateTime**](\DateTime.md) |  | 
**text** | **string** |  | 
**receiver** | **string** |  | 
**status** | **string** |  | 
**firstName** | **string** |  | 
**lastName** | **string** |  | 
**sessionId** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


