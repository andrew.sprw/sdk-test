# SendMessageResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**href** | **string** |  | 
**type** | **string** |  | 
**sessionId** | **int** |  | 
**bulkId** | **int** |  | 
**messageId** | **int** |  | 
**scheduleId** | **int** |  | 
**chatId** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


