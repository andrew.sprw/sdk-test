# UnsubscribeContactInputObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phone** | **string** | Contact phone number | [optional] 
**blockIncoming** | **int** | Default is 0 (false). If set to 1 incoming messages from this number will be blocked. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


