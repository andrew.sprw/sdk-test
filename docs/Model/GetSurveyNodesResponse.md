# GetSurveyNodesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nodes** | [**\text-magic\Model\SurveyNode[]**](SurveyNode.md) |  | 
**rows** | [**int[][]**](array.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


