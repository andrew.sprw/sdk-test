# CreateSenderIdInputObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**senderId** | **string** | Alphanumeric Sender ID (maximum 11 characters) | 
**explanation** | **string** | Explain why do you need this Sender ID | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


