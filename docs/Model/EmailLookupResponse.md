# EmailLookupResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **string** |  | 
**addressType** | **string** |  | 
**emailRole** | **string** |  | 
**reason** | **string** |  | 
**status** | **string** |  | 
**deliverability** | **string** |  | 
**isDisposableAddress** | **bool** |  | 
**localPart** | **string** |  | 
**domainPart** | **string** |  | 
**exchange** | **string** |  | 
**isInWhiteList** | **bool** |  | 
**isInBlackList** | **bool** |  | 
**hasMx** | **bool** |  | 
**hasAa** | **bool** |  | 
**hasAaaa** | **bool** |  | 
**risk** | **string** |  | 
**preference** | **int** |  | 
**suggestion** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


