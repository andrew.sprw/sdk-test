# UpdateListObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | List name | 
**shared** | **bool** | Should this list be shared with sub-accounts | 
**favorited** | **bool** | Is list favorited. Default is false | [optional] [default to false]
**isDefault** | **bool** | Is list default for new contacts (web only). | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


