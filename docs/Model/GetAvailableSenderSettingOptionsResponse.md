# GetAvailableSenderSettingOptionsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dedicated** | **string[]** |  | 
**user** | **string[]** |  | 
**shared** | **string[]** |  | 
**senderIds** | **string[]** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


