# UsersInbound

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**user** | [**\text-magic\Model\User**](User.md) |  | 
**purchasedAt** | [**\DateTime**](\DateTime.md) |  | 
**expireAt** | [**\DateTime**](\DateTime.md) |  | 
**status** | **string** | A - active, in use (at least one message was sent/received from/to this number), U - never used before | 
**country** | [**\text-magic\Model\Country**](Country.md) |  | 
**phone** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


