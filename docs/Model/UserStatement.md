# UserStatement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**userId** | **int** |  | 
**date** | [**\DateTime**](\DateTime.md) |  | 
**balance** | **double** |  | 
**delta** | **float** |  | 
**type** | **string** |  | 
**value** | **string** |  | 
**comment** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


