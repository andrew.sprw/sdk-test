# Contact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**favorited** | **bool** |  | 
**blocked** | **bool** |  | 
**firstName** | **string** |  | 
**lastName** | **string** |  | 
**companyName** | **string** |  | 
**phone** | **string** |  | 
**email** | **string** |  | 
**country** | [**\text-magic\Model\Country**](Country.md) |  | 
**customFields** | [**\text-magic\Model\ContactCustomField[]**](ContactCustomField.md) |  | 
**user** | [**\text-magic\Model\User**](User.md) |  | 
**lists** | [**\text-magic\Model\Group[]**](Group.md) |  | 
**phoneType** | **string** |  | 
**avatar** | [**\text-magic\Model\ContactImage**](ContactImage.md) |  | 
**notes** | [**\text-magic\Model\ContactNote[]**](ContactNote.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


