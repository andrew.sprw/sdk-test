# UpdatePasswordInputObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**oldPassword** | **string** | Current password | 
**newPassword** | **string** | New password | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


