# GetCarrierLookupResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cost** | **float** |  | 
**country** | [**\text-magic\Model\Country**](Country.md) |  | [optional] 
**local** | **string** |  | 
**type** | **string** |  | 
**carrier** | **string** |  | 
**number164** | **string** |  | 
**valid** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


