# GetMessagingCountersResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contacts** | **int** |  | 
**sent** | **int** |  | 
**received** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


