# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**username** | **string** |  | 
**firstName** | **string** |  | 
**lastName** | **string** |  | 
**email** | **string** |  | 
**status** | **string** |  | 
**balance** | **float** |  | 
**phone** | **string** |  | 
**company** | **string** |  | 
**currency** | [**\text-magic\Model\Currency**](Currency.md) |  | 
**country** | [**\text-magic\Model\Country**](Country.md) |  | 
**timezone** | [**\text-magic\Model\Timezone**](Timezone.md) |  | 
**subaccountType** | **string** |  | 
**emailAccepted** | **bool** |  | 
**phoneAccepted** | **bool** |  | 
**avatar** | [**\text-magic\Model\UserImage**](UserImage.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


