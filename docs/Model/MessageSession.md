# MessageSession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**startTime** | **string** |  | 
**text** | **string** |  | 
**source** | **string** |  | 
**referenceId** | **string** |  | 
**price** | **float** |  | 
**numbersCount** | **int** |  | 
**destination** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


