# GetMessageSessionStatResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**failed** | **int** |  | 
**delivered** | **int** |  | 
**accepted** | **int** |  | 
**rejected** | **int** |  | 
**scheduled** | **int** |  | 
**all** | **int** |  | 
**sent** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


