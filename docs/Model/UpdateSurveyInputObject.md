# UpdateSurveyInputObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Survey name | 
**contacts** | **string** | Array of contact resources id message will be sent to | [optional] 
**lists** | **string** | Array of list resources id message will be sent to | [optional] 
**phones** | **string** | Array of E.164 phone numbers message will be sent to | [optional] 
**country** | **object** | Country values mapping (country &#x3D;&gt; inbound phone id), e.g. country[GB] &#x3D; \&quot;123\&quot;, country[US] &#x3D; \&quot;123\&quot; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


