# MessageOut

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**contactId** | **int** |  | 
**sessionId** | **int** |  | 
**receiver** | **string** |  | [optional] 
**messageTime** | [**\DateTime**](\DateTime.md) |  | 
**status** | **object** | q - queued s - scheduled queue e - sending error r - enroute a - acked d - delivered b - buffered f - failed u - unknown j - rejected i - bulk insert p - scheduled suspend h - queue suspend | 
**avatar** | **string** |  | 
**text** | **string** |  | 
**deleted** | **bool** |  | [optional] 
**charset** | **string** |  | 
**charsetLabel** | **string** |  | 
**firstName** | **string** |  | 
**lastName** | **string** |  | 
**country** | **string** |  | 
**sender** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**price** | **float** |  | [optional] 
**partsCount** | **int** |  | 
**fromEmail** | **string** |  | [optional] 
**fromNumber** | **string** |  | [optional] 
**smscId** | **int** |  | [optional] 
**contact** | **string** |  | [optional] 
**source** | **string** |  | [optional] 
**deliveredCount** | **int** |  | [optional] 
**numbersCount** | **int** |  | [optional] 
**userId** | **int** |  | [optional] 
**creditsPrice** | **float** |  | [optional] 
**chars** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


