# ForwardedCall

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**callTime** | [**\DateTime**](\DateTime.md) |  | 
**caller** | **string** |  | 
**via** | **string** | Inbound id | 
**receiver** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


