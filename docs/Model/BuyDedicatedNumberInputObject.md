# BuyDedicatedNumberInputObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phone** | **string** | Desired dedicated phone number in international E.164 format | 
**country** | **string** | Dedicated number country. Two charactes in upper case. | 
**userId** | **int** | User ID this number will be assigned to | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


