# MessagesIcsParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **string** |  | 
**recipients** | [**\text-magic\Model\MessagesIcsParametersRecipients**](MessagesIcsParametersRecipients.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


