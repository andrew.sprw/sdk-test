# MessagesIcsTextParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cost** | **float** |  | 
**parts** | **int** |  | 
**chars** | **int** |  | 
**encoding** | **string** |  | 
**countries** | **string[]** |  | 
**charsetLabel** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


