# Group

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**name** | **string** |  | 
**description** | **string** |  | 
**favorited** | **bool** |  | 
**membersCount** | **int** |  | 
**user** | [**\text-magic\Model\User**](User.md) |  | 
**service** | **bool** |  | 
**shared** | **bool** |  | 
**avatar** | [**\text-magic\Model\GroupImage**](GroupImage.md) |  | 
**isDefault** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


