# MessagingStatItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replyRate** | **float** |  | 
**date** | [**\DateTime**](\DateTime.md) |  | 
**deliveryRate** | **float** |  | 
**costs** | **float** |  | 
**messagesReceived** | **int** |  | 
**messagesSentDelivered** | **int** |  | 
**messagesSentAccepted** | **int** |  | 
**messagesSentBuffered** | **int** |  | 
**messagesSentFailed** | **int** |  | 
**messagesSentRejected** | **int** |  | 
**messagesSentParts** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


