# MessageIn

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**sender** | **string** |  | 
**receiver** | **string** |  | 
**messageTime** | [**\DateTime**](\DateTime.md) |  | 
**text** | **string** |  | 
**contactId** | **int** |  | [optional] 
**firstName** | **string** |  | [optional] 
**lastName** | **string** |  | [optional] 
**avatar** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


