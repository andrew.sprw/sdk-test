# MergeSurveyNodesInputObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstNode** | **int** | First node id | 
**secondNode** | **int** | Second node id | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


