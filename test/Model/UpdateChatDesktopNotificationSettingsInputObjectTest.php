<?php
/**
 * UpdateChatDesktopNotificationSettingsInputObjectTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * TextMagic
 *
 * TextMagic REST API
 *
 * OpenAPI spec version: 2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.7
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace text-magic;

/**
 * UpdateChatDesktopNotificationSettingsInputObjectTest Class Doc Comment
 *
 * @category    Class
 * @description UpdateChatDesktopNotificationSettingsInputObject
 * @package     text-magic
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class UpdateChatDesktopNotificationSettingsInputObjectTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "UpdateChatDesktopNotificationSettingsInputObject"
     */
    public function testUpdateChatDesktopNotificationSettingsInputObject()
    {
    }

    /**
     * Test attribute "playSound"
     */
    public function testPropertyPlaySound()
    {
    }

    /**
     * Test attribute "showNotifications"
     */
    public function testPropertyShowNotifications()
    {
    }

    /**
     * Test attribute "showText"
     */
    public function testPropertyShowText()
    {
    }

    /**
     * Test attribute "soundId"
     */
    public function testPropertySoundId()
    {
    }
}
