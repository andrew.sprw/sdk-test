<?php
/**
 * MessageInTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * TextMagic
 *
 * TextMagic REST API
 *
 * OpenAPI spec version: 2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.7
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace text-magic;

/**
 * MessageInTest Class Doc Comment
 *
 * @category    Class
 * @description MessageIn
 * @package     text-magic
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class MessageInTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "MessageIn"
     */
    public function testMessageIn()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "sender"
     */
    public function testPropertySender()
    {
    }

    /**
     * Test attribute "receiver"
     */
    public function testPropertyReceiver()
    {
    }

    /**
     * Test attribute "messageTime"
     */
    public function testPropertyMessageTime()
    {
    }

    /**
     * Test attribute "text"
     */
    public function testPropertyText()
    {
    }

    /**
     * Test attribute "contactId"
     */
    public function testPropertyContactId()
    {
    }

    /**
     * Test attribute "firstName"
     */
    public function testPropertyFirstName()
    {
    }

    /**
     * Test attribute "lastName"
     */
    public function testPropertyLastName()
    {
    }

    /**
     * Test attribute "avatar"
     */
    public function testPropertyAvatar()
    {
    }
}
