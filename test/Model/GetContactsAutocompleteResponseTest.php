<?php
/**
 * GetContactsAutocompleteResponseTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * TextMagic
 *
 * TextMagic REST API
 *
 * OpenAPI spec version: 2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.7
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace text-magic;

/**
 * GetContactsAutocompleteResponseTest Class Doc Comment
 *
 * @category    Class
 * @description GetContactsAutocompleteResponse
 * @package     text-magic
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class GetContactsAutocompleteResponseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "GetContactsAutocompleteResponse"
     */
    public function testGetContactsAutocompleteResponse()
    {
    }

    /**
     * Test attribute "entityId"
     */
    public function testPropertyEntityId()
    {
    }

    /**
     * Test attribute "entityType"
     */
    public function testPropertyEntityType()
    {
    }

    /**
     * Test attribute "value"
     */
    public function testPropertyValue()
    {
    }

    /**
     * Test attribute "label"
     */
    public function testPropertyLabel()
    {
    }

    /**
     * Test attribute "sharedBy"
     */
    public function testPropertySharedBy()
    {
    }

    /**
     * Test attribute "avatar"
     */
    public function testPropertyAvatar()
    {
    }

    /**
     * Test attribute "favorited"
     */
    public function testPropertyFavorited()
    {
    }

    /**
     * Test attribute "userId"
     */
    public function testPropertyUserId()
    {
    }

    /**
     * Test attribute "countryName"
     */
    public function testPropertyCountryName()
    {
    }

    /**
     * Test attribute "qposition"
     */
    public function testPropertyQposition()
    {
    }

    /**
     * Test attribute "rposition"
     */
    public function testPropertyRposition()
    {
    }
}
