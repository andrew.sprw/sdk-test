<?php
/**
 * OutboundMessageSessionsApiTest
 * PHP version 5
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * TextMagic
 *
 * TextMagic REST API
 *
 * OpenAPI spec version: 2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.7
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace text-magic;

use \text-magic\Configuration;
use \text-magic\ApiException;
use \text-magic\ObjectSerializer;

/**
 * OutboundMessageSessionsApiTest Class Doc Comment
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class OutboundMessageSessionsApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for deleteMessageSession
     *
     * Delete a message session, together with all nested messages..
     *
     */
    public function testDeleteMessageSession()
    {
    }

    /**
     * Test case for deleteMessageSessionsBulk
     *
     * Delete messages sessions, together with all nested messages, by given ID(s) or delete all messages sessions..
     *
     */
    public function testDeleteMessageSessionsBulk()
    {
    }

    /**
     * Test case for getAllMessageSessions
     *
     * Get all message sending sessions..
     *
     */
    public function testGetAllMessageSessions()
    {
    }

    /**
     * Test case for getMessageSession
     *
     * Get a message session..
     *
     */
    public function testGetMessageSession()
    {
    }

    /**
     * Test case for getMessageSessionStat
     *
     * Get sending session statistics..
     *
     */
    public function testGetMessageSessionStat()
    {
    }

    /**
     * Test case for getMessagesBySessionId
     *
     * Fetch messages by given session id..
     *
     */
    public function testGetMessagesBySessionId()
    {
    }
}
