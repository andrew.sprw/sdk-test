<?php
/**
 * IntegrationApiTest
 * PHP version 5
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * TextMagic
 *
 * TextMagic REST API
 *
 * OpenAPI spec version: 2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.7
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace text-magic;

use \text-magic\Configuration;
use \text-magic\ApiException;
use \text-magic\ObjectSerializer;

/**
 * IntegrationApiTest Class Doc Comment
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class IntegrationApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createPushToken
     *
     * Add or update a device token..
     *
     */
    public function testCreatePushToken()
    {
    }

    /**
     * Test case for deletePushToken
     *
     * Delete a push notification device token..
     *
     */
    public function testDeletePushToken()
    {
    }

    /**
     * Test case for getPushTokens
     *
     * Get all device tokens assigned to the current account.
     *
     */
    public function testGetPushTokens()
    {
    }
}
