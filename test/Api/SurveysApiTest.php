<?php
/**
 * SurveysApiTest
 * PHP version 5
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * TextMagic
 *
 * TextMagic REST API
 *
 * OpenAPI spec version: 2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.7
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace text-magic;

use \text-magic\Configuration;
use \text-magic\ApiException;
use \text-magic\ObjectSerializer;

/**
 * SurveysApiTest Class Doc Comment
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class SurveysApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for apiV2SurveysGet
     *
     * Get all user surveys..
     *
     */
    public function testApiV2SurveysGet()
    {
    }

    /**
     * Test case for cancelSurvey
     *
     * Cancel a survey..
     *
     */
    public function testCancelSurvey()
    {
    }

    /**
     * Test case for createSurvey
     *
     * Create a new survey from the submitted data..
     *
     */
    public function testCreateSurvey()
    {
    }

    /**
     * Test case for createSurveyNode
     *
     * Create a new node from the submitted data..
     *
     */
    public function testCreateSurveyNode()
    {
    }

    /**
     * Test case for deleteSurvey
     *
     * Delete a survey..
     *
     */
    public function testDeleteSurvey()
    {
    }

    /**
     * Test case for deleteSurveyNode
     *
     * Delete a node..
     *
     */
    public function testDeleteSurveyNode()
    {
    }

    /**
     * Test case for duplicateSurvey
     *
     * Duplicate a survey..
     *
     */
    public function testDuplicateSurvey()
    {
    }

    /**
     * Test case for getSurvey
     *
     * Get a survey by id..
     *
     */
    public function testGetSurvey()
    {
    }

    /**
     * Test case for getSurveyNode
     *
     * Get a node by id..
     *
     */
    public function testGetSurveyNode()
    {
    }

    /**
     * Test case for getSurveyNodes
     *
     * Fetch nodes by given survey id..
     *
     */
    public function testGetSurveyNodes()
    {
    }

    /**
     * Test case for mergeSurveyNodes
     *
     * Merge two question nodes..
     *
     */
    public function testMergeSurveyNodes()
    {
    }

    /**
     * Test case for resetSurvey
     *
     * Reset a survey flow..
     *
     */
    public function testResetSurvey()
    {
    }

    /**
     * Test case for startSurvey
     *
     * Start a survey..
     *
     */
    public function testStartSurvey()
    {
    }

    /**
     * Test case for updateSurvey
     *
     * Update existing survey..
     *
     */
    public function testUpdateSurvey()
    {
    }

    /**
     * Test case for updateSurveyNode
     *
     * Update existing node..
     *
     */
    public function testUpdateSurveyNode()
    {
    }
}
