<?php
/**
 * ChatsApiTest
 * PHP version 5
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * TextMagic
 *
 * TextMagic REST API
 *
 * OpenAPI spec version: 2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.7
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace text-magic;

use \text-magic\Configuration;
use \text-magic\ApiException;
use \text-magic\ObjectSerializer;

/**
 * ChatsApiTest Class Doc Comment
 *
 * @category Class
 * @package  text-magic
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ChatsApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for closeChatsBulk
     *
     * Close chats by chat ids or close all chats.
     *
     */
    public function testCloseChatsBulk()
    {
    }

    /**
     * Test case for closeReadChats
     *
     * Close all chats that have no unread messages..
     *
     */
    public function testCloseReadChats()
    {
    }

    /**
     * Test case for deleteChatMessages
     *
     * Delete messages from chat by given messages ID(s)..
     *
     */
    public function testDeleteChatMessages()
    {
    }

    /**
     * Test case for deleteChatsBulk
     *
     * Delete chats by given ID(s) or delete all chats..
     *
     */
    public function testDeleteChatsBulk()
    {
    }

    /**
     * Test case for getAllChats
     *
     * Get all user chats..
     *
     */
    public function testGetAllChats()
    {
    }

    /**
     * Test case for getChat
     *
     * Get a single chat..
     *
     */
    public function testGetChat()
    {
    }

    /**
     * Test case for getChatByPhone
     *
     * Find chats by phone..
     *
     */
    public function testGetChatByPhone()
    {
    }

    /**
     * Test case for getChatMessages
     *
     * Fetch messages from chat with specified chat id..
     *
     */
    public function testGetChatMessages()
    {
    }

    /**
     * Test case for getUnreadMessagesTotal
     *
     * Get total amount of unread messages in the current user chats..
     *
     */
    public function testGetUnreadMessagesTotal()
    {
    }

    /**
     * Test case for markChatsReadBulk
     *
     * Mark several chats as read by chat ids or mark all chats as read.
     *
     */
    public function testMarkChatsReadBulk()
    {
    }

    /**
     * Test case for markChatsUnreadBulk
     *
     * Mark several chats as UNread by chat ids or mark all chats as UNread.
     *
     */
    public function testMarkChatsUnreadBulk()
    {
    }

    /**
     * Test case for muteChat
     *
     * Set mute mode..
     *
     */
    public function testMuteChat()
    {
    }

    /**
     * Test case for muteChatsBulk
     *
     * Mute several chats by chat ids or mute all chats.
     *
     */
    public function testMuteChatsBulk()
    {
    }

    /**
     * Test case for reopenChatsBulk
     *
     * Reopen chats by chat ids or reopen all chats.
     *
     */
    public function testReopenChatsBulk()
    {
    }

    /**
     * Test case for searchChats
     *
     * Find chats by inbound or outbound messages text..
     *
     */
    public function testSearchChats()
    {
    }

    /**
     * Test case for searchChatsByIds
     *
     * Find chats by IDs..
     *
     */
    public function testSearchChatsByIds()
    {
    }

    /**
     * Test case for searchChatsByReceipent
     *
     * Find chats by recipient (contact, list name or phone number)..
     *
     */
    public function testSearchChatsByReceipent()
    {
    }

    /**
     * Test case for setChatStatus
     *
     * Set status of the chat given by ID..
     *
     */
    public function testSetChatStatus()
    {
    }

    /**
     * Test case for unmuteChatsBulk
     *
     * Unmute several chats by chat ids or unmute all chats.
     *
     */
    public function testUnmuteChatsBulk()
    {
    }
}
